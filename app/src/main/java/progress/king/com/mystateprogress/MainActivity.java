package progress.king.com.mystateprogress;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import progress.king.com.mystateprogress.view.MyStateProgress;

public class MainActivity extends AppCompatActivity {

    MyStateProgress mp_2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView(){
        mp_2 = (MyStateProgress) findViewById(R.id.mp_2);
        mp_2.setPes(20,16,0,12);
    }

}
